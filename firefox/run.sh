#!/bin/bash
SCRIPTDIR=$(cd $(dirname $0) ; pwd)
cd ${SCRIPTDIR}
[ ! -d ${SCRIPTDIR}/containerdata ] ; mkdir -p ${SCRIPTDIR}/containerdata
[ ! -f ~/.local/share/applications/containerized-firefox.desktop ] ; cp containerized-firefox.desktop ~/.local/share/applications/containerized-firefox.desktop

if [ "$1" = "-d" ] || [ "$1" = "--debug" ]
then
  BUILD_SILENT=""
  RUN_DEBUG=bash
  RUN_BACKGROUND='--user 0'
else
  BUILD_SILENT="-q"
  RUN_BACKGROUND='-d'
fi

if [ "${BUILD_SILENT}" = "-q" ]
then
  docker build --pull $BUILD_SILENT -t containerenv/path/$(basename $(pwd)) . | zenity --progress --pulsate --auto-close --no-cancel --text "" --title "Building Container"
else
  docker build --pull -t containerenv/path/$(basename $(pwd)) .
fi

# Add video devices
VIDEODEVICES=""
for VIDEODEV in $(ls /dev/video*)
do
  if [ "${VIDEODEV}" != "" ]
  then
    VIDEODEVICES="--device $VIDEODEV ${VIDEODEVICES}"
  fi
done

if [ $? = 0 ]
then
  docker rm -f $(basename $(pwd))
  docker run $RUN_BACKGROUND -ti --rm \
    --security-opt label=type:container_runtime_t \
    -e DISPLAY=$DISPLAY \
    -v /run/user/${UID}/pulse:/run/user/${UID}/pulse \
    -v ${SCRIPTDIR}/containerdata:/home/appuser:z \
    -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 \
    -v /dev/shm:/dev/shm \
    -v /etc/machine-id:/etc/machine-id \
    $VIDEODEVICES \
    --name $(basename $(pwd)) \
    containerenv/path/$(basename $(pwd)) $RUN_DEBUG
    #containerenv/path/$(basename $(pwd)) $RUN_DEBUG | zenity --progress --pulsate --auto-close --no-cancel --text "" --title "Starting Application"
fi

#docker run -e XDG_RUNTIME_DIR=/tmp -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY -v $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY containerenv/path/$(basename $(pwd)) $(basename $(pwd))
